import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:image_picker/image_picker.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:login_signup/views/profile/jobs_form_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/job_posted.dart';

List<JobPosted> lsJobPosted = [];

Future<List<JobPosted>> getJobPostedDirect() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String strPosteds = prefs.getString('jobs') ?? '';

  List<JobPosted> localLsJobPosted = [];
  if (strPosteds != '') {
    var list = jsonDecode(strPosteds);
    List mapJobPosted = list['data'];
    localLsJobPosted = mapJobPosted
        .map((e) => JobPosted.fromMap(e))
        .toList();
  }

  return localLsJobPosted;
}

class ProfileJobsScreen extends StatefulWidget {
  const ProfileJobsScreen({Key? key}) : super(key: key);

  @override
  _ProfileJobsScreenState createState() => _ProfileJobsScreenState();
}

class _ProfileJobsScreenState extends State<ProfileJobsScreen> {
  getJobPosted() async {
    var result = await getJobPostedDirect();
    if (mounted) {
      setState(() {
        lsJobPosted = result;
      });
    }
  }

  Widget futureBuilder() {
    return FutureBuilder<String>(builder: (context, snapshot) {
      if (lsJobPosted.isNotEmpty) {
        return TaskList(jobPosteds: lsJobPosted);
      }

      return const Text('');
    });
  }

  @override
  void initState() {
    super.initState();
    getJobPosted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: const Color(0x00759cbf),
            title: const Text('Jobs Posted'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
        ),
        body: Stack(
          children: <Widget>[
            futureBuilder(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const JobsFormScreen();
            })).then((value) =>
                getJobPosted()
            );
          },
          backgroundColor: Colors.green,
          child: const Icon(Icons.add),
        ),
    );
  }

  dynamic mi(PickedFile? _imageFile) {
    if (_imageFile == null) {
      return const AssetImage("image/baby.png");
    } else {
      return FileImage(File(_imageFile.path));
    }
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.jobPosteds}) : super(key: key);

  final List<JobPosted> jobPosteds;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 60, left: 10),
        child: ListView.builder(
          itemCount: jobPosteds.length,
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Container(
              padding: const EdgeInsets.only(
                  left: 14, right: 14, top: 10, bottom: 10),
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.kLightOrange,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        jobPosteds[index].name,
                        style: const TextStyle(
                            fontSize: 20
                        ),
                        textAlign: TextAlign.left,
                      ),
                      const Divider(
                        height: 15,
                        color: Colors.black,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        jobPosteds[index].description,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 12
                        ),
                        textAlign: TextAlign.left,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Duration: ' + jobPosteds[index].duration,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 12
                        ),
                        textAlign: TextAlign.left,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Skills: ' + jobPosteds[index].skills,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 12
                        ),
                        textAlign: TextAlign.left,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Salary: ' + jobPosteds[index].salary,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 12
                        ),
                        textAlign: TextAlign.left,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Conditions: ' + jobPosteds[index].conditions,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 12
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
