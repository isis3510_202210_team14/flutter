import 'package:login_signup/utils/exports.dart';

class AppColors {
  static const kBlueColor = Color.fromARGB(0, 240, 135, 6);
  static const kBlackColor = Color(0xFF000000);
  static const kwhiteColor = Color(0xFFFFFFFF);
  static const kDarkblack = Color.fromARGB(255, 197, 234, 252);
  static const kOrangeColor = Color.fromRGBO(242, 135, 5, 1);
  static const kLightBlueColor = Color.fromRGBO(117, 156, 191, 1);
  static const kLightOrange = Color.fromARGB(255, 238, 188, 126);
  static const kBluee = Color.fromARGB(255, 9, 10, 78);
}
