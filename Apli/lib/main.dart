import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'dart:async';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runZonedGuarded(() {
    runApp(AppState());
  }, FirebaseCrashlytics.instance.recordError);
}

class AppState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService()),
        ChangeNotifierProvider(create: (_) => AuthService()),
        ChangeNotifierProvider(create: (_) => BasicInfoService()),
        ChangeNotifierProvider(create: (_) => JobsService()),
        ChangeNotifierProvider(create: (_) => Jobs2Service()),
        ChangeNotifierProvider(create: (_) => Jobs3Service()),
        ChangeNotifierProvider(create: (_) => UserService()),
        ChangeNotifierProvider(create: (_) => ConnectionStatusModel()),
      ],
      child: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Auto Theme App',
      initialRoute: 'checking',
      routes: {
        'checking': (_) => CheckAuthScreen(),
        'home': (_) => HomeScreen(),
        'jobs': (_) => MainScreen(),
        'jobs2': (_) => Main2Screen(),
        'jobs3': (_) => Main3Screen(),
      },
      scaffoldMessengerKey: NotificationsService.messengerKey,
      theme: ThemeData(
        scaffoldBackgroundColor: AppColors.kDarkblack,
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
        scaffoldBackgroundColor: AppColors.kBluee,
        brightness: Brightness.dark,
      ),
      home: HomeScreen(),
    );
  }
}
