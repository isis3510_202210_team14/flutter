import 'package:login_signup/utils/exports.dart';
import '../models/models.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

DatabaseReference ref2 = FirebaseDatabase.instance.ref();

class Jobs2Service extends ChangeNotifier {
  final storageToken = new FlutterSecureStorage();

  List<Job> jobs = [];

  bool isLoading = false;

  late Job selectedJob;

  Jobs2Service() {
    this.loadJobs();
    //this.refresh();
  }

  Future<List<Job>> refresh() async {
    this.isLoading = true;
    notifyListeners();

    ref2.child("Jobs").onValue.listen((event) {
      final data = Map<dynamic, dynamic>.from(event.snapshot.value as dynamic);
      this.jobs = [];
      data.forEach((key, value) {
        final aux = Job.fromMap(value);
        aux.id = key;
        this.jobs.add(aux);
      });
    });

    this.isLoading = false;
    notifyListeners();

    return this.jobs;
  }

  Future<List<Job>> loadJobs() async {
    this.isLoading = true;
    notifyListeners();

    DatabaseReference ref = FirebaseDatabase.instance.ref();

    final snapshot = await ref.child('Jobs').get();
    if (snapshot.exists) {
      final jobsMap = Map<dynamic, dynamic>.from(snapshot.value as dynamic);
      jobsMap.forEach((key, value) {
        final aux = Job.fromMap(value);
        aux.id = key;
        this.jobs.add(aux);
      });
    } else {
      print('No data available.');
    }

    this.isLoading = false;
    notifyListeners();

    return this.jobs;
  }

  Future saveNewApplicant(Job job) async {
    final UID = await storageToken.read(key: 'UID');
    List list = [];
    DatabaseReference ref = FirebaseDatabase.instance.ref();
    if (job.applicants != null) {
      list = job.applicants!.toList();
      list.add(UID);
    }
    ref.child('Jobs/${job.id}').update({
      "Applicants": job.applicants == null ? [UID] : list
    });
  }
}
