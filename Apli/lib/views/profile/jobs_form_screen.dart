import 'dart:convert';
import 'dart:isolate';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:login_signup/models/job_posted.dart';
import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

const String _baseURL = 'backend-workapp-default-rtdb.firebaseio.com';

Future<String> getUID() async {
  const storage = FlutterSecureStorage();
  return await storage.read(key: 'UID') ?? '';
}

setListMessagesIsolate(JobPosted jobPosted) async {
  ReceivePort port = ReceivePort();
  String uid = await getUID();
  final isolate = await Isolate.spawn<List<dynamic>>(
      setListMessages, [port.sendPort, uid, jobPosted]);
  final messages = await port.first;
  isolate.kill(priority: Isolate.immediate);
  return messages;
}

void setListMessages(List<dynamic> values) async {
  SendPort sendPort = values[0];
  String uid = values[1];
  JobPosted jobPosted = values[2];

  final url = Uri.https(_baseURL, 'JobsPosted/' + uid + '.json');
  final resp = await http.post(url, body: jobPosted.toJson());
  final Map<String, dynamic> messageMap = json.decode(resp.body);

  sendPort.send(messageMap);
}

setJobPosted(Map<String, dynamic> job) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String strPosteds = prefs.getString('jobs') ?? '';

  if (strPosteds != '') {
    var jobs = jsonDecode(strPosteds);

    List mapJobs = jobs['data'];
    mapJobs.add(job);

    JobPosted jobPosted = JobPosted.fromMap(job);
    setListMessagesIsolate(jobPosted);

    await prefs.setString('jobs', jsonEncode(jobs));
  } else {
    Map<String, dynamic> jobs = {
      'data': [job],
    };

    await prefs.setString('jobs', jsonEncode(jobs));
  }
}

class JobsFormScreen extends StatefulWidget {
  const JobsFormScreen({Key? key}) : super(key: key);

  @override
  _JobsFormScreenState createState() => _JobsFormScreenState();
}

class _JobsFormScreenState extends State<JobsFormScreen> {
  final _formKey = GlobalKey<FormState>();

  late String _name;
  late String _duration;
  late String _description;
  late String _skills;
  late String _salary;
  late String _conditions;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: const Color(0x00759cbf),
            title: const Text('Jobs Form'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
        ),
        body: Form(
          key: _formKey,
          child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              children: <Widget>[
                Row(
                  children: [
                    Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                      return model.isOnline
                          ? const Text("")
                          : const AlertDialog(
                              title: Text("Out of internet connection"),
                              content: Text("The profile may be disconnected"),
                              elevation: 24.0,
                              backgroundColor: Colors.orange,
                            );
                    }),
                  ],
                ),
                TextFormField(
                  decoration: setInputDecoration("Company Name"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _name = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: setInputDecoration("Description"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _description = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: setInputDecoration("Duration in the job"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _duration = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: setInputDecoration("Skills"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _skills = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: setInputDecoration("Salary"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _salary = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: setInputDecoration("Conditions"),
                  // autofocus: true,
                  keyboardType: TextInputType.multiline,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() { _conditions = value; });
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                InkWell(
                  child: SignUpContainer(st: 'Submit'),
                  onTap: () async {
                    if (_formKey.currentState!.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Processing Data')),
                      );

                      Map<String, dynamic> job = {
                        'name': _name,
                        'description': _description,
                        'duration': _duration,
                        'skills': _skills,
                        'salary': _salary,
                        'conditions': _conditions,
                      };

                      setJobPosted(job);

                      Navigator.pop(context);
                    }
                  },
                ),
              ]
          ),
        ),
    );
  }

  InputDecoration setInputDecoration(String tag) {
    return InputDecoration(
        labelText: tag,
        labelStyle: const TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.normal,
          fontSize: 17,
        ),
        hintText: "Write " + tag,
        hintStyle: const TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.normal,
          fontSize: 17,
        ),
        border: const OutlineInputBorder(
            borderSide: BorderSide(
              width: 5,
              color: Colors.black,
              style: BorderStyle.solid,
            )
        )
    );
  }
}
