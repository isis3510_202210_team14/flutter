import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
//import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';

class CVScreen extends StatefulWidget {
  const CVScreen({Key? key}) : super(key: key);

  @override
  _CVState createState() => _CVState();
}

class _CVState extends State<CVScreen> {
  bool aux = false;
  PickedFile? _imageFile;
  final ImagePicker _picker = ImagePicker();

  final storageToken = new FlutterSecureStorage();

  FirebaseStorage storage = FirebaseStorage.instance;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: customText(
                          txt: "COMPLETE YOUR PROFILE",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.orange)),
                    ),
                    const SizedBox(
                      height: 30,
                    ),

                    Center(
                      child: ScreenProgress2(
                        ticks: 2,
                      ),
                    ),

                    SizedBox(
                      height: MediaQuery.of(context).size.height >
                              MediaQuery.of(context).size.width
                          ? 110
                          : 30,
                    ),

                    Row(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.height >
                                  MediaQuery.of(context).size.width
                              ? 0
                              : 150,
                        ),
                        InkWell(
                          onTap: () async {
                            //final path =
                            // await FlutterDocumentPicker.openDocument();
                            //File file = File(path);
                            //uploadFile(file);
                          },
                          child: aux == false
                              ? Icon(
                                  Icons.file_upload,
                                  color: Colors.grey,
                                  size: 160.0,
                                )
                              : Icon(
                                  Icons.download_done,
                                  color: Colors.grey,
                                  size: 160.0,
                                ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.height >
                                  MediaQuery.of(context).size.width
                              ? 10
                              : 90,
                        ),
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: ((builder) => bottomSheet()));
                          },
                          child: Stack(children: <Widget>[
                            CircleAvatar(
                              radius: 80.0,
                              // ignore: unnecessary_null_comparison
                              backgroundImage: mi(_imageFile),
                              backgroundColor: AppColors.kDarkblack,
                            ),
                            Positioned(
                              bottom: -4.0,
                              right: 10.0,
                              child: InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      builder: ((builder) => bottomSheet()));
                                },
                                child: Icon(
                                  Icons.add_photo_alternate,
                                  color: Colors.teal,
                                  size: 20.0,
                                ),
                              ),
                            ),
                          ]),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.height >
                                  MediaQuery.of(context).size.width
                              ? 53
                              : 205,
                        ),
                        Text('Upload CV'),
                        SizedBox(
                          width: MediaQuery.of(context).size.height >
                                  MediaQuery.of(context).size.width
                              ? 80
                              : 157,
                        ),
                        Text('Upload Image'),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height >
                              MediaQuery.of(context).size.width
                          ? 110
                          : 50,
                    ),
                    InkWell(
                      child: SignUpContainer(st: "Next"),
                      onTap: () async {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const FinishScreen()));
                      },
                    ),

                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<UploadTask?> uploadFile(File file) async {
    if (file == null) {
      return null;
    }

    final UID = await storageToken.read(key: 'UID');

    final path = 'CV/' + UID!;

    final ref = FirebaseStorage.instance.ref().child(path);

    UploadTask uploadTask = ref.putFile(file);

    showModalBottomSheet(
        context: context, builder: ((builder) => bottomSheetFile()));
    aux = true;
    (context as Element).reassemble();
  }

  dynamic mi(PickedFile? _imageFile) {
    if (_imageFile == null) {
      return AssetImage("image/baby.png");
    } else {
      return FileImage(File(_imageFile.path));
    }
  }

  Widget bottomSheetFile() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Row(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          Text(
            "File uploaded successfuly!",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Icon(Icons.check_circle, color: Colors.blue, size: 40.0)
        ],
      ),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 110.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          Text(
            "Choose Profile photo",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.camera),
              onPressed: () {
                takePhoto(ImageSource.camera);
              },
              label: Text("Camera"),
            ),
            FlatButton.icon(
              icon: Icon(Icons.image),
              onPressed: () {
                takePhoto(ImageSource.gallery);
              },
              label: Text("Gallery"),
            ),
          ])
        ],
      ),
    );
  }

  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile!;
    });

    final UID = await storageToken.read(key: 'UID');

    final path = 'profileImages/' + UID!;

    final file = File(_imageFile!.path);

    final ref = FirebaseStorage.instance.ref().child(path);

    UploadTask uploadTask = ref.putFile(file);

    var dowurl =
        (await uploadTask.whenComplete(() => null)).ref.getDownloadURL();
    var url = dowurl.toString();

    image(url);
  }

  void image(String url) async {
    final userService = Provider.of<UserService>(context, listen: false);

    final String? errorMessage = await userService.changeImageUser(url);
  }
}

class ScreenProgress2 extends StatelessWidget {
  final int ticks;

  ScreenProgress2({required this.ticks});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        spacer2(context),
        tick1(),
        spacer(),
        line(context),
        spacer(),
        tick2(),
        spacer(),
        line(context),
        spacer(),
        tick3(),
      ],
    );
  }

  Widget tick(bool isChecked) {
    return isChecked
        ? Icon(Icons.check_circle, color: Colors.blue, size: 40.0)
        : Icon(Icons.radio_button_unchecked, color: Colors.blue, size: 40.0);
  }

  Widget tick1() {
    return this.ticks > 0 ? tick(true) : tick(false);
  }

  Widget tick2() {
    return this.ticks > 1 ? tick(true) : tick(false);
  }

  Widget tick3() {
    return this.ticks > 2 ? tick(true) : tick(false);
  }

  Widget tick4() {
    return this.ticks > 3 ? tick(true) : tick(false);
  }

  Widget spacer() {
    return Container(
      width: 5.0,
    );
  }

  Widget spacer2(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.height >
                MediaQuery.of(context).size.width
            ? 50
            : 170);
  }

  Widget line(BuildContext context) {
    return Container(
      color: Colors.blue,
      height: 5.0,
      width:
          MediaQuery.of(context).size.height > MediaQuery.of(context).size.width
              ? 70
              : 150,
    );
  }
}
