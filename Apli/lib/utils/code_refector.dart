import 'package:login_signup/utils/exports.dart';

// custom text widget
Widget customText(
    {required String txt, required TextStyle style, TextAlign? align}) {
  return Text(txt, style: style, textAlign: align);
}

// inkwell buttons pic
Widget InkwellButtons({
  required Image image,
}) {
  return Expanded(
    child: Container(
      width: 170,
      height: 60,
      child: image,
    ),
  );
}

// sign up button
Widget SignUpContainer({required String st}) {
  return Container(
    width: double.infinity,
    height: 60,
    decoration: BoxDecoration(
      color: Colors.orange,
      borderRadius: BorderRadius.circular(12),
    ),
    child: Center(
      child: customText(
          txt: st,
          style: const TextStyle(
            color: AppColors.kwhiteColor,
            fontWeight: FontWeight.normal,
            fontSize: 14,
          )),
    ),
  );
}

Widget ReloadContainer({required String st}) {
  return Container(
    width: 300,
    height: 60,
    decoration: BoxDecoration(
      color: Colors.grey,
      borderRadius: BorderRadius.circular(12),
    ),
    child: Center(
      child: customText(
          txt: st,
          style: const TextStyle(
            color: AppColors.kBlackColor,
            fontWeight: FontWeight.normal,
            fontSize: 14,
          )),
    ),
  );
}

Widget CustomTextField(
    {required String Lone, required String Htwo, bool? obs}) {
  return TextFormField(
    obscureText: obs == null ? false : obs,
    validator: (value) {
      if (Lone == 'Email') {
        String pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regExp = new RegExp(pattern);

        return regExp.hasMatch(value ?? '')
            ? null
            : 'The enter value is not an email';
      } else {
        return (value != null && value.length >= 6)
            ? null
            : 'The password must have at least 6 characters';
      }
    },
    decoration: InputDecoration(
        labelText: Lone,
        hintText: Htwo,
        hintStyle: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
        border: const OutlineInputBorder(
            borderSide: BorderSide(
          width: 5,
          color: AppColors.kBlackColor,
          style: BorderStyle.solid,
        ))),
    autofocus: true,
    keyboardType: TextInputType.multiline,
  );
}

// rich text
TextSpan RichTextSpan({required String one, required String two}) {
  return TextSpan(children: [
    TextSpan(text: one, style: TextStyle(fontSize: 17)),
    TextSpan(
        text: two,
        style: TextStyle(
          fontSize: 17,
          color: Colors.orange,
        )),
  ]);
}

// TextField
Widget CustomTextFieldReg(
    {required String Lone,
    required String Htwo,
    bool? obs,
    required dynamic LoginForm}) {
  return TextFormField(
    obscureText: obs == null ? false : obs,
    onChanged: (value) {
      if (Lone == 'Email') {
        LoginForm.email = value;
      } else if (Lone == 'Confirm Password') {
        LoginForm.password2 = value;
      } else {
        LoginForm.password = value;
      }
    },
    validator: (value) {
      if (Lone == 'Email') {
        String pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regExp = new RegExp(pattern);

        return regExp.hasMatch(value ?? '')
            ? null
            : 'The enter value is not an email';
      } else {
        return (value != null && value.length >= 6)
            ? null
            : 'The password must have at least 6 characters';
      }
    },
    decoration: InputDecoration(
        labelText: Lone,
        hintText: Htwo,
        hintStyle: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
        border: const OutlineInputBorder(
            borderSide: BorderSide(
          width: 5,
          color: AppColors.kBlackColor,
          style: BorderStyle.solid,
        ))),
    autofocus: true,
    keyboardType: TextInputType.multiline,
  );
}

Widget CustomTextFielsBasicInfo(
    {required String Lone,
    required String Htwo,
    bool? obs,
    required dynamic LoginForm}) {
  return TextFormField(
    obscureText: obs == null ? false : obs,
    onChanged: (value) {
      if (Lone == 'Name') {
        LoginForm.name = value;
      } else if (Lone == 'Age') {
        LoginForm.age = value;
      } else if (Lone == 'Study level') {
        LoginForm.study = value;
      } else if (Lone == 'Phone number') {
        LoginForm.phone = value;
      } else if (Lone == 'Location') {
        LoginForm.location = value;
      }
    },
    decoration: InputDecoration(
        labelText: Lone,
        hintText: Htwo,
        hintStyle: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
        border: const OutlineInputBorder(
            borderSide: BorderSide(
          width: 5,
          color: AppColors.kBlackColor,
          style: BorderStyle.solid,
        ))),
    autofocus: true,
    keyboardType: TextInputType.multiline,
  );
}
