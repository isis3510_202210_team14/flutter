import 'package:login_signup/models/users.dart';

class UserPreferences{
  static const myUser = User(
    imagePath: '',
    name: 'Sofia Puentes',
    email: 's.puentes@gmail.com',
    about: 'Hi, my name is Sofía and I am a recently graduated student  in Universidad de los Andes.',
    isDarkMode: false,);
}