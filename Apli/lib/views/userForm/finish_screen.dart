import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class FinishScreen extends StatefulWidget {
  const FinishScreen({Key? key}) : super(key: key);

  @override
  _FinishState createState() => _FinishState();
}

class _FinishState extends State<FinishScreen> {
  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context, listen: true);
    final storage = new FlutterSecureStorage();

    //if (userService.isLoading) return LoadingScreen();

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: customText(
                          txt: "COMPLETE YOUR PROFILE",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.orange)),
                    ),
                    const SizedBox(
                      height: 30,
                    ),

                    Center(
                      child: ScreenProgress3(
                        ticks: 3,
                      ),
                    ),

                    const SizedBox(
                      height: 30,
                    ),

                    Center(
                        child: Column(
                      children: [
                        Row(children: [
                          const SizedBox(
                            width: 80,
                          ),
                          Text('NAME: '),
                          const SizedBox(
                            width: 40,
                          ),
                          Text(userService.user.name),
                        ]),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(children: [
                          const SizedBox(
                            width: 80,
                          ),
                          Text('AGE: '),
                          const SizedBox(
                            width: 40,
                          ),
                          Text(userService.user.age),
                        ]),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(children: [
                          const SizedBox(
                            width: 80,
                          ),
                          Text('EMAIL: '),
                          const SizedBox(
                            width: 40,
                          ),
                          Text(userService.user.email),
                        ]),
                        const SizedBox(
                          height: 50,
                        ),
                        InkWell(
                          child: SignUpContainer(st: "Next"),
                          onTap: () async {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const WelcomeScreen()));
                          },
                        ),
                      ],
                    ))

                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ScreenProgress3 extends StatelessWidget {
  final int ticks;

  ScreenProgress3({required this.ticks});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        spacer2(context),
        tick1(),
        spacer(),
        line(context),
        spacer(),
        tick2(),
        spacer(),
        line(context),
        spacer(),
        tick3(),
      ],
    );
  }

  Widget tick(bool isChecked) {
    return isChecked
        ? Icon(Icons.check_circle, color: Colors.blue, size: 40.0)
        : Icon(Icons.radio_button_unchecked, color: Colors.blue, size: 40.0);
  }

  Widget tick1() {
    return this.ticks > 0 ? tick(true) : tick(false);
  }

  Widget tick2() {
    return this.ticks > 1 ? tick(true) : tick(false);
  }

  Widget tick3() {
    return this.ticks > 2 ? tick(true) : tick(false);
  }

  Widget tick4() {
    return this.ticks > 3 ? tick(true) : tick(false);
  }

  Widget spacer() {
    return Container(
      width: 5.0,
    );
  }

  Widget spacer2(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.height >
                MediaQuery.of(context).size.width
            ? 50
            : 190);
  }

  Widget line(BuildContext context) {
    return Container(
      color: Colors.blue,
      height: 5.0,
      width:
          MediaQuery.of(context).size.height > MediaQuery.of(context).size.width
              ? 70
              : 150,
    );
  }
}
