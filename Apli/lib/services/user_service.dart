import 'package:login_signup/utils/exports.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../models/models.dart';
import 'dart:convert';

class UserService extends ChangeNotifier {
  final String _baseURL = 'backend-workapp-default-rtdb.firebaseio.com';
  bool isLoading = true;
  UserService() {
    getClient();
  }
  Client user = Client(name: '', age: '', phone: '', location: '', email: '');

  final storage = new FlutterSecureStorage();

  Future getClient() async {
    isLoading = true;
    notifyListeners();

    final id = await storage.read(key: 'UID');

    final url = Uri.https(_baseURL, 'Users/' + id! + '.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp = await http.get(url);
    final Map<String, dynamic> usersMap = json.decode(resp.body);

    user = Client.fromMap(usersMap);

    isLoading = false;
    notifyListeners();
    return user;
  }

  Future createUser(Client user) async {
    final url = Uri.https(_baseURL, 'Users.json');
    final resp = await http.post(url, body: user.toJson());
    final decodedData = resp.body;

    return '';
  }

  Future changeImageUser(String imageURL) async {
    final id = await storage.read(key: 'UID');

    final url = Uri.https(_baseURL, 'Users/' + id! + '.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    Client client = Client(
        name: decodedData['Name'],
        age: decodedData['Age'],
        study: decodedData['Study'],
        phone: decodedData['Phone'],
        location: decodedData['Location'],
        email: decodedData['Email'],
        image: imageURL);

    final url2 = Uri.https(_baseURL, 'Users/' + id + '.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp2 = await http.put(url2, body: client.toJson());
  }
}
