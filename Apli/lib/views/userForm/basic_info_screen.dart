import 'package:login_signup/providers/basic_form_provider.dart';
import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';

class BasicInfoScreen extends StatefulWidget {
  const BasicInfoScreen({Key? key}) : super(key: key);

  @override
  _BasicInfoState createState() => _BasicInfoState();
}

class _BasicInfoState extends State<BasicInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: customText(
                          txt: "COMPLETE YOUR PROFILE",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.orange)),
                    ),
                    const SizedBox(
                      height: 30,
                    ),

                    Center(
                      child: ScreenProgress(
                        ticks: 1,
                      ),
                    ),

                    const SizedBox(
                      height: 30,
                    ),

                    ChangeNotifierProvider(
                        create: (_) => BasicFormProvider(),
                        child: _BasicForm()),

                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _BasicForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final basicForm = Provider.of<BasicFormProvider>(context);
    return Container(
        child: Form(
            key: basicForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                CustomTextFielsBasicInfo(
                    Lone: "Name",
                    Htwo: "Your complete name",
                    LoginForm: basicForm),
                const SizedBox(height: 20),
                CustomTextFielsBasicInfo(
                    Lone: "Age", Htwo: "Age", LoginForm: basicForm),
                const SizedBox(height: 20),
                CustomTextFielsBasicInfo(
                    Lone: "Study level",
                    Htwo: "Study level",
                    LoginForm: basicForm),
                const SizedBox(height: 20),
                CustomTextFielsBasicInfo(
                    Lone: "Phone number",
                    Htwo: "Phone number",
                    LoginForm: basicForm),
                const SizedBox(height: 20),
                CustomTextFielsBasicInfo(
                    Lone: "Location",
                    Htwo: "Your location",
                    LoginForm: basicForm),
                const SizedBox(height: 40),
                InkWell(
                  child: SignUpContainer(st: "Next"),
                  onTap: () async {
                    final formService =
                        Provider.of<BasicInfoService>(context, listen: false);

                    basicForm.isLoading = true;

                    final String? errorMessage =
                        await formService.basicInfoForm(
                            basicForm.name,
                            basicForm.age,
                            basicForm.study,
                            basicForm.phone,
                            basicForm.location);
                    if (errorMessage == null) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const CVScreen()));
                    } else {
                      if (errorMessage == "INVALID_EMAIL") {
                        NotificationsService.showSnackBar('Invalid email');
                      } else if (errorMessage == "MISSING_PASSWORD") {
                        NotificationsService.showSnackBar('Missing password');
                      } else if (errorMessage == "EMAIL_NOT_FOUND") {
                        NotificationsService.showSnackBar('Email not found');
                      } else if (errorMessage == "INVALID_PASSWORD") {
                        NotificationsService.showSnackBar('Invalid password');
                      } else {
                        NotificationsService.showSnackBar(errorMessage);
                      }
                    }
                  },
                ),
              ],
            )));
  }
}

class ScreenProgress extends StatelessWidget {
  final int ticks;

  ScreenProgress({required this.ticks});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        spacer2(context),
        tick1(),
        spacer(),
        line(context),
        spacer(),
        tick2(),
        spacer(),
        line(context),
        spacer(),
        tick3(),
      ],
    );
  }

  Widget tick(bool isChecked) {
    return isChecked
        ? Icon(Icons.check_circle, color: Colors.blue, size: 40.0)
        : Icon(Icons.radio_button_unchecked, color: Colors.blue, size: 40.0);
  }

  Widget tick1() {
    return this.ticks > 0 ? tick(true) : tick(false);
  }

  Widget tick2() {
    return this.ticks > 1 ? tick(true) : tick(false);
  }

  Widget tick3() {
    return this.ticks > 2 ? tick(true) : tick(false);
  }

  Widget tick4() {
    return this.ticks > 3 ? tick(true) : tick(false);
  }

  Widget spacer() {
    return Container(
      width: 5.0,
    );
  }

  Widget spacer2(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.height >
                MediaQuery.of(context).size.width
            ? 50
            : 190);
  }

  Widget line(BuildContext context) {
    return Container(
      color: Colors.blue,
      height: 5.0,
      width:
          MediaQuery.of(context).size.height > MediaQuery.of(context).size.width
              ? 70
              : 150,
    );
  }
}
