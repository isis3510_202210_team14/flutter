export 'package:flutter/material.dart';

//Views
export 'package:login_signup/views/home_screen.dart';
export 'package:login_signup/views/loading_screen.dart';
export 'package:login_signup/views/login_signup/login_screen.dart';
export 'package:login_signup/views/lookingJob/search_screen.dart';
export 'package:login_signup/views/main/main_screen.dart';
export 'package:login_signup/views/main/main2_screen.dart';
export 'package:login_signup/views/main/main3_screen.dart';
export 'package:login_signup/views/messages/messages_screen.dart';
export 'package:login_signup/views/profile/profile_screen.dart';
export 'package:login_signup/views/check_auth_screen.dart';
export 'package:login_signup/views/userForm/basic_info_screen.dart';
export 'package:login_signup/views/userForm/CV_screen.dart';
export 'package:login_signup/views/userForm/finish_screen.dart';
//export 'package:login_signup/views/login_signup/signup_sreen.dart';
export 'package:login_signup/views/welcome_Screen.dart';
export 'package:login_signup/views/lookingJob/search2_screen.dart';
export 'package:login_signup/views/lookingJob/search3_screen.dart';
export 'package:login_signup/views/lookingJob/job_detail_screen.dart';
export 'package:login_signup/views/lookingJob/job2_detail_screen.dart';
export 'package:login_signup/views/lookingJob/job3_detail_screen.dart';
export 'package:login_signup/widgets/job_detail_widget.dart';
export 'package:login_signup/views/lookingJob/applied_screen.dart';
export 'package:login_signup/views/lookingJob/applied2_screen.dart';
export 'package:login_signup/views/lookingJob/applied3_screen.dart';

//Controllers

//Utils
export 'package:login_signup/utils/code_refector.dart';
export 'appColors.dart';

//Models
