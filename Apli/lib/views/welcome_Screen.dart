import 'package:login_signup/utils/exports.dart';
import 'package:login_signup/services/services.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 44),
            child: Column(
              children: [
                Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                  return model.isOnline
                      ? Column(children: [
                          const Image(image: AssetImage("image/Workapp.png")),
                          const SizedBox(height: 48),
                          customText(
                              txt: "Thank You",
                              style: const TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                              )),
                          const SizedBox(height: 8),
                          customText(
                              txt: "Welcome to our beautiful app!",
                              style: const TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                              )),
                          const SizedBox(height: 60),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 0, horizontal: 15),
                            child: InkWell(
                              child: SignUpContainer(st: "Let's Go"),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => const MainScreen()));
                              },
                            ),
                          ),
                        ])
                      : Center(
                          child: Column(
                          children: [
                            SizedBox(height: 150),
                            CircularProgressIndicator(
                              color: Colors.indigo,
                            ),
                            SizedBox(height: 80),
                            Center(
                                child: Text(
                              'Oh oh, it seems there is no internet conection',
                              style: Theme.of(context).textTheme.headline3,
                              textAlign: TextAlign.center,
                            )),
                          ],
                        ));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
