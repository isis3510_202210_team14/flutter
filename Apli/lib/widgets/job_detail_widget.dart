import 'package:cached_network_image/cached_network_image.dart';

import 'package:login_signup/utils/exports.dart';

class JobDetailWidget extends StatelessWidget {
  final String? image;

  const JobDetailWidget({Key? key, this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 10, right: 20, top: 2),
        child: Container(
            decoration: BoxDecoration(
                color: AppColors.kDarkblack,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(45),
                    topRight: Radius.circular(45)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      blurRadius: 10,
                      offset: Offset(0, 5))
                ]),
            width: MediaQuery.of(context).size.height >
                    MediaQuery.of(context).size.width
                ? double.infinity
                : 500,
            height: 400,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(45), topRight: Radius.circular(45)),
              child: image != null
                  ? CachedNetworkImage(
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      imageUrl: image.toString(),
                      fit: BoxFit.fill,
                    )
                  : Image.asset("image/no-photo.png", height: 70),
            )));
  }
}
