import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import '../../models/client.dart';
import '../../models/message.dart';

const String _baseURL = 'backend-workapp-default-rtdb.firebaseio.com';

List<Message> lsMessages = [];

Future<String> getUID() async {
  const storage = FlutterSecureStorage();
  return await storage.read(key: 'UID') ?? '';
}

Future createMessage(Client userTo, String messageText) async {
  String uid = await getUID();
  DateTime dateSend = DateTime.now();

  Message messageFrom = Message(
    fromUser: uid,
    toUser: userTo.id.toString(),
    content: messageText,
    type: 'sender',
    dateSend: dateSend,
  );
  messageFrom.id = await writeMessage(messageFrom);
  writeLocalStorageMessage(messageFrom);

  Message messageTo = Message(
    fromUser: userTo.id.toString(),
    toUser: uid,
    content: messageText,
    type: 'receiver',
    dateSend: dateSend,
  );
  messageTo.id = await writeMessage(messageTo);

  return;
}

Future<String> writeMessage(Message message) async {
  final url = Uri.https(_baseURL, 'Message/' + message.fromUser + '.json');
  final resp = await http.post(url, body: message.toJson());
  final Map<String, dynamic> messageMap = json.decode(resp.body);

  return messageMap['name'];
}

Future writeLocalStorageMessage(Message message) async {
  final directories =
      await getExternalStorageDirectories(type: StorageDirectory.downloads);
  String path1 = '';
  for (Directory directory in directories!) {
    if (!directory.path.contains('/storage/emulated/0/')) {
      path1 = directory.path;
    }
  }

  if (path1.isNotEmpty) {
    final file_name = message.id;
    File file = File('$path1/$file_name.txt');

    await file.writeAsString(message.content);
  }

  return;
}

Future<List<Message>> getListMessagesIsolate(String id) async {
  ReceivePort port = ReceivePort();
  String uid = await getUID();
  final isolate = await Isolate.spawn<List<dynamic>>(
      getListMessages, [port.sendPort, id, uid]);
  final messages = await port.first;
  isolate.kill(priority: Isolate.immediate);
  return messages;
}

void getListMessages(List<dynamic> values) async {
  SendPort sendPort = values[0];
  String id = values[1];
  String uid = values[2];

  List<Message> list = [];

  final url = Uri.https(_baseURL, 'Message/' + uid + '.json');
  final resp = await http.get(url);

  if (resp.body != 'null') {
    final Map<String, dynamic> usersMap = json.decode(resp.body);

    // usersMap.entries.forEach((e) => list.add(Client.fromMap(e.value)));
    for (var e in usersMap.entries) {
      Message client = Message.fromMap(e.value);
      if (client.toUser == id) {
        client.id = e.key;
        list.add(client);
      }
    }
  }

  sendPort.send(list);
}

class ChatDetailPage extends StatefulWidget {
  const ChatDetailPage({Key? key, required this.user}) : super(key: key);

  final Client user;

  @override
  _ChatDetailPageState createState() => _ChatDetailPageState();
}

class _ChatDetailPageState extends State<ChatDetailPage> {
  getClientImage() {
    if (_user.image != '') {
      return CircleAvatar(
        backgroundImage: NetworkImage(_user.image.toString()),
        maxRadius: 20,
      );
    } else {
      return const CircleAvatar(
        backgroundImage: AssetImage("image/baby.png"),
        maxRadius: 20,
      );
    }
  }

  getMessages() async {
    var result = await getListMessagesIsolate(_user.id.toString());
    //var result = await getListMessages(_user.id.toString());
    if (mounted) {
      setState(() {
        lsMessages = result;
      });
    }
  }

  void clearText() {
    fieldText.clear();
    getMessages();
  }

  late final Client _user = widget.user;
  String textMessage = '';
  bool _isRunning = true;
  final fieldText = TextEditingController();

  Widget futureBuilder() {
    return FutureBuilder<String>(builder: (context, snapshot) {
      if (lsMessages.isNotEmpty) {
        return TaskList(messages: lsMessages);
      }

      return const Text('');
    });
  }

  @override
  void initState() {
    Timer.periodic(const Duration(seconds: 5), (Timer timer) {
      if (!_isRunning) {
        timer.cancel();
      }
      getMessages();
    });
    getMessages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0x00759cbf),
        centerTitle: true,
        flexibleSpace: SafeArea(
          child: Container(
            padding: const EdgeInsets.only(right: 16),
            child: Row(
              children: <Widget>[
                IconButton(
                  onPressed: () {
                    setState(() {
                      _isRunning = false;
                    });
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                ),
                /*IconButton(
                  onPressed: () async {
                    getMessages();
                  },
                  icon: const Icon(
                    Icons.refresh,
                    color: Colors.black,
                  ),
                ),*/
                const SizedBox(
                  width: 2,
                ),
                getClientImage(),
                const SizedBox(
                  width: 12,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _user.name,
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        'ID; ' + _user.id.toString(),
                        style:
                            TextStyle(color: Colors.grey.shade600, fontSize: 8),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          futureBuilder(),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
              height: 60,
              width: double.infinity,
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          hintText: "Escribir mensaje...",
                          hintStyle: TextStyle(color: Colors.black54),
                          border: InputBorder.none),
                      controller: fieldText,
                      onChanged: (text) {
                        setState(() {
                          textMessage = text;
                        });
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      createMessage(_user, textMessage);
                      clearText();
                    },
                    child: const Icon(
                      Icons.send,
                      color: Colors.white,
                      size: 18,
                    ),
                    backgroundColor: Colors.blue,
                    elevation: 0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.messages}) : super(key: key);

  final List<Message> messages;

  String getTime(DateTime dateSend) {
    return DateFormat.jm().format(dateSend);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 60, left: 10),
        child: ListView.builder(
          itemCount: messages.length,
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Container(
              padding: const EdgeInsets.only(
                  left: 14, right: 14, top: 10, bottom: 10),
              child: Align(
                alignment: (messages[index].type == "receiver"
                    ? Alignment.topLeft
                    : Alignment.topRight),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: (messages[index].type == "receiver"
                        ? Colors.grey.shade200
                        : Colors.blue[200]),
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: (messages[index].type == "receiver"
                        ? CrossAxisAlignment.start
                        : CrossAxisAlignment.end),
                    children: [
                      Text(
                        messages[index].content,
                        style: const TextStyle(fontSize: 15),
                        textAlign: (messages[index].type == "receiver"
                            ? TextAlign.left
                            : TextAlign.right),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        getTime(messages[index].dateSend),
                        style: TextStyle(
                            color: Colors.grey.shade600, fontSize: 11),
                        textAlign: (messages[index].type == "receiver"
                            ? TextAlign.left
                            : TextAlign.right),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
