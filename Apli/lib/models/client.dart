import 'dart:convert';

class Client {
  Client(
      {required this.name,
      required this.age,
      this.study,
      required this.phone,
      required this.location,
      required this.email,
      this.image});

  String name;
  String age;
  String? study;
  String phone;
  String location;
  String? image;
  String? id;
  String email;

  factory Client.fromJson(String str) => Client.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Client.fromMap(Map<String, dynamic> json) => Client(
      name: json["Name"] ?? '',
      age: json["Age"] ?? '',
      study: json["Study"] ?? '',
      phone: json["Phone"] ?? '',
      location: json["Location"] ?? '',
      image: json["Image"] ?? '',
      email: json["Email"] ?? '');

  Map<String, dynamic> toMap() => {
        "Name": name,
        "Age": age,
        "Study": study,
        "Phone": phone,
        "Location": location,
        "Image": image,
        "Email": email
      };
}
