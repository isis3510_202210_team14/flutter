// ignore_for_file: deprecated_member_use

import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:login_signup/models/users.dart';
import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:login_signup/utils/user_preferences.dart';
import 'package:login_signup/views/profile/profile_jobs_screen.dart';
import 'package:login_signup/widgets/numbers_widget.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final user = UserPreferences.myUser;
  PickedFile? _imageFile;
  final ImagePicker _picker = ImagePicker();
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
            backgroundColor: const Color(0x00759cbf),
            title: const Text('Your profile'),
            leading: IconButton(
              icon: const Icon(Icons.login_outlined),
              onPressed: () {
                authService.logout();
                Navigator.pushReplacementNamed(context, 'home');
              },
            )),
        body: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            children: <Widget>[
              imageProfile(),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                    return model.isOnline
                        ? const Text("")
                        : const AlertDialog(
                            title: Text("Out of internet connection"),
                            content: Text("The profile may be disconnected"),
                            elevation: 24.0,
                            backgroundColor: Colors.orange,
                          );
                  }),
                ],
              ),
              const SizedBox(height: 24),
              buildName(user),
              const SizedBox(height: 24),
              NumbersWidget(),
              const SizedBox(height: 24),
              buildAbout(user),
              const SizedBox(height: 24),
              buildButtons(user),
            ]));
  }

  dynamic mi(PickedFile? _imageFile) {
    if (_imageFile == null) {
      return const AssetImage("image/baby.png");
    } else {
      return FileImage(File(_imageFile.path));
    }
  }

  Widget imageProfile() {
    //String ? image = us.imagePath;
    return Center(
      child: Stack(children: <Widget>[
        CircleAvatar(
          radius: 80.0,
          backgroundImage: mi(_imageFile),
          backgroundColor: AppColors.kDarkblack,
        ),
        /* image != ''
                  ? CachedNetworkImage(
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      imageUrl: image.toString(),
                      height: 70)
                  : Image.asset("image/gallery.png", height: 70),
              const SizedBox(
                height: 10,
              ),*/
        Positioned(
          bottom: -4.0,
          right: 10.0,
          child: InkWell(
            onTap: () {
              showModalBottomSheet(
                  context: context, builder: ((builder) => bottomSheet()));
            },
            child: const Icon(
              Icons.camera_alt,
              color: Colors.teal,
              size: 40.0,
            ),
          ),
        ),
      ]),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          const Text(
            "Choose Profile photo",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            FlatButton.icon(
              icon: const Icon(Icons.camera),
              onPressed: () {
                takePhoto(ImageSource.camera);
              },
              label: const Text("Camera"),
            ),
            FlatButton.icon(
              icon: const Icon(Icons.image),
              onPressed: () {
                takePhoto(ImageSource.gallery);
              },
              label: const Text("Gallery"),
            ),
          ])
        ],
      ),
    );
  }
/*
  String _path;
  File _cachedFile;

  Future<Null> downloadFile(String httpPath) async {
    final RegExp regExp = RegExp('([^?/]*\.(jpg))');
    final String fileName = regExp.stringMatch(httpPath);
    final Directory tempDir = Directory.systemTemp;
    final File file = File('${tempDir.path}/$fileName');

    final StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
    final StorageFileDownloadTask downloadTask = ref.writeToFile(file);

    final int byteNumber = (await downloadTask.future).totalByteCount;

    print(byteNumber);

    setState(() => _cachedFile = file);
  }
*/

  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile!;
    });
  }

  Widget buildName(User user) => Column(
        children: [
          Text(
            user.name,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            user.email,
            style: const TextStyle(color: Colors.grey),
          ),
        ],
      );

  Widget buildAbout(User user) => Container(
        padding: const EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'About',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Text(
              user.about,
              style: const TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      );

  Widget buildButtons(User user) => Container(
        padding: const EdgeInsets.symmetric(horizontal: 48),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 4,
              child: ElevatedButton(
                child: const Text('Historical'),
                onPressed: () {},
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 4,
              child: ElevatedButton(
                child: const Text('Jobs Posted'),
                onPressed: () async {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const ProfileJobsScreen();
                  }));
                },
              ),
            ),
          ],
        ),
      );
}
