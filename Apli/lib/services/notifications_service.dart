import 'package:flutter/material.dart';

class NotificationsService {
  static GlobalKey<ScaffoldMessengerState> messengerKey =
      new GlobalKey<ScaffoldMessengerState>();

  static showSnackBar(String message) {
    final snackBar = new SnackBar(
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(5.00),
        ),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.grey,
      content: Text(message,
          style: TextStyle(
            fontSize: 25,
            color: Colors.white,
          )),
    );

    messengerKey.currentState!.showSnackBar(snackBar);
  }
}
