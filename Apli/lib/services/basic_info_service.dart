import 'package:flutter/material.dart';
import '../models/models.dart';
import 'package:login_signup/models/client.dart';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';

class BasicInfoService extends ChangeNotifier {
  final String _baseURL = 'backend-workapp-default-rtdb.firebaseio.com';
  final List<Client> users = [];
  bool isLoading = true;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  late User currentuser;
  bool isloggedin = false;

  final storage = new FlutterSecureStorage();

  int? stringToInt_tryParse(String input) {
    return int.tryParse(input);
  }

  Future<String?> basicInfoForm(String name, String age, String study,
      String phone, String location) async {
    final email = await storage.read(key: 'email');
    final Map<String, dynamic> authData = {
      'name': name,
      'age': age,
      'study': study,
      'phone': phone,
      'locaion': location,
      'email': email
    };

    Client client = Client(
        name: name,
        age: age,
        study: study,
        phone: phone,
        location: location,
        email: email!);

    if (name.isEmpty) {
      return "The name can not be an empty space";
    }

    if (age.isEmpty) {
      return "The age can not be an empty space";
    }

    if (age.length > 2 || stringToInt_tryParse(age) == null) {
      return "The age must be a valid age";
    }

    if (study.length <= 3 && !study.isEmpty) {
      return "The study level mus be a valid data";
    }

    if (phone.isEmpty) {
      return "The phone number can not be an empty space";
    }

    if (phone.length < 5 || stringToInt_tryParse(phone) == null) {
      return "The phone number must be valid";
    }

    final UID = await storage.read(key: 'UID');

    final url = Uri.https(_baseURL, 'Users/' + UID! + '.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp = await http.put(url, body: client.toJson());
    final decodedData = json.decode(resp.body);

    client.id = UID;

    return null;
  }
}
