import 'dart:convert';

class JobPosted {
  String name;
  String description;
  String duration;
  String skills;
  String salary;
  String conditions;
  String? image;
  String? id;

  JobPosted({
    required this.name,
    required this.description,
    required this.duration,
    required this.skills,
    required this.salary,
    required this.conditions,
    this.image,
  });

  factory JobPosted.fromJson(String str) => JobPosted.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory JobPosted.fromMap(Map<dynamic, dynamic> json) => JobPosted(
    name: json["name"],
    description: json["description"],
    duration: json["duration"],
    skills: json["skills"],
    salary: json["salary"],
    conditions: json["conditions"],
  );

  Map<dynamic, dynamic> toMap() => {
    "name": name,
    "description": description,
    "duration": duration,
    "skills": skills,
    "salary": salary,
    "conditions": conditions,
  };
}
