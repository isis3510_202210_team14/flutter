import 'package:login_signup/utils/exports.dart';

class AppliedScreen extends StatefulWidget {
  const AppliedScreen({Key? key}) : super(key: key);

  @override
  _AppliedScreenState createState() => _AppliedScreenState();
}

class _AppliedScreenState extends State<AppliedScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 44),
            child: Column(
              children: [
                Column(children: [
                  const Image(image: AssetImage("image/Workapp.png")),
                  const SizedBox(height: 48),
                  customText(
                      txt: "Great!",
                      style: const TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                      )),
                  const SizedBox(height: 30),
                  Image.asset(
                    "image/search.png",
                    width: 120,
                  ),
                  const SizedBox(height: 25),
                  customText(
                      txt: "Continue looking for other jobs!",
                      style: const TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 20,
                      )),
                  const SizedBox(height: 50),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                    child: InkWell(
                      child: SignUpContainer(st: "See other jobs"),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const Main2Screen()));
                      },
                    ),
                  ),
                ])
              ],
            ),
          ),
        ),
      ),
    );
  }
}
