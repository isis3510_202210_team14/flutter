import 'dart:convert';

class Message {
  Message(
      {required this.fromUser,
      required this.toUser,
      required this.content,
      required this.type,
      required this.dateSend});

  String? id;
  String fromUser;
  String toUser;
  String content;
  String type;
  DateTime dateSend;

  factory Message.fromJson(String str) => Message.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Message.fromMap(Map<String, dynamic> json) => Message(
        fromUser: json["FromUser"],
        toUser: json["ToUser"],
        content: json["Content"],
        type: json["Type"],
        dateSend: DateTime.parse(json["DateSend"] as String),
      );

  Map<String, dynamic> toMap() => {
        "FromUser": fromUser,
        "ToUser": toUser,
        "Content": content,
        "Type": type,
        "DateSend": dateSend.toString(),
      };
}
