// To parse this JSON data, do
//
//     final jobs = jobsFromMap(jsonString);

import 'dart:convert';

import 'dart:ffi';

class Job {
  Job(
      {required this.description,
      this.image,
      required this.name,
      required this.time,
      this.id,
      required this.salary,
      this.applicants});

  String description;
  String? image;
  String name;
  String time;
  String? id;
  int salary;
  List? applicants;

  factory Job.fromJson(String str) => Job.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Job.fromMap(Map<dynamic, dynamic> json) => Job(
        description: json["Description"],
        image: json["Image"],
        name: json["Name"],
        time: json["Time"],
        salary: json["Salary"],
        applicants: json["Applicants"],
      );

  Map<dynamic, dynamic> toMap() => {
        "Description": description,
        "Image": image,
        "Name": name,
        "Time": time,
        "Salary": salary,
        "Applicants": applicants,
      };

  Job copy() => Job(
      description: this.description,
      image: this.image,
      name: this.name,
      time: this.time,
      id: this.id,
      salary: this.salary,
      applicants: this.applicants);
}
