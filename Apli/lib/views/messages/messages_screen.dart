import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:login_signup/models/client.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:login_signup/services/services.dart';
import '../../services/connection_service.dart';
import 'chatDetailPage.dart';

const String _baseURL = 'backend-workapp-default-rtdb.firebaseio.com';

Future<String> getUID() async {
  const storage = FlutterSecureStorage();
  return await storage.read(key: 'UID') ?? '';
}

Future<List<Client>> getListUsers() async {
  String uid = await getUID();
  final url = Uri.https(_baseURL, 'Users.json');
  final resp = await http.get(url);
  final Map<String, dynamic> usersMap = json.decode(resp.body);

  List<Client> list = [];
  // usersMap.entries.forEach((e) => list.add(Client.fromMap(e.value)));
  Client client;
  for (var e in usersMap.entries) {
    if (e.key != uid) {
      client = Client.fromMap(e.value);
      client.id = e.key;
      list.add(client);
    }
  }

  return list;
}

class MessagesScreen extends StatefulWidget {
  const MessagesScreen({Key? key}) : super(key: key);

  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0x00759cbf),
        title: const Text('Search'),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                getListUsers();
              });
            },
            icon: const Icon(Icons.search),
          )
        ],
        centerTitle: true,
      ),
      body: Consumer<ConnectionStatusModel>(builder: (_, model, __) {
        return model.isOnline
            ? FutureBuilder<List<Client>>(
                future: getListUsers(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return const Center(
                      child: Text('An error has occurred!'),
                    );
                  } else if (snapshot.hasData) {
                    return TaskList(userSummarys: snapshot.data!);
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              )
            : Center(
                child: Column(
                children: [
                  const SizedBox(height: 150),
                  CircularProgressIndicator(
                    color: Colors.indigo,
                  ),
                  const SizedBox(height: 60),
                  Center(
                      child: Text(
                    'Oh oh,it seems there is no internet connection',
                    style: Theme.of(context).textTheme.headline3,
                    textAlign: TextAlign.center,
                    textScaleFactor: 0.7,
                  )),
                ],
              ));
      }),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.userSummarys}) : super(key: key);

  final List<Client> userSummarys;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        // padding: const EdgeInsets.all(5),
        itemCount: userSummarys.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
            userSummary: userSummarys[index],
          ));
        });
  }
}

class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.userSummary}) : super(key: key);
  final Client userSummary;

  getClientImage() {
    if (userSummary.image != '') {
      return Image.network(userSummary.image.toString(), height: 100);
    } else {
      return Image.asset("image/baby.png", height: 100);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: InkWell(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: AppColors.kLightOrange,
          elevation: 10,
          child: Column(
            children: <Widget>[
              Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                return model.isOnline
                    ? Row(children: <Widget>[
                        Container(
                          margin: const EdgeInsets.all(10),
                          child: getClientImage(),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Text(userSummary.name,
                                    style: const TextStyle(fontSize: 20.0),
                                    textAlign: TextAlign.left),
                                const SizedBox(
                                  height: 10,
                                ),
                                customText(
                                    txt: userSummary.age,
                                    style: const TextStyle(
                                        fontSize: 15,
                                        color: AppColors.kBlackColor)),
                              ],
                            ),
                          ),
                        ),
                      ])
                    : Center(
                        child: Column(
                        children: [
                          const CircularProgressIndicator(
                            color: Colors.indigo,
                          ),
                          const SizedBox(height: 50),
                          Center(
                              child: Text(
                            'Oh oh, it seems there is no internet connection',
                            style: Theme.of(context).textTheme.headline3,
                            textAlign: TextAlign.center,
                          )),
                        ],
                      ));
              }),
            ],
          ),
        ),
        splashColor: Colors.blueGrey,
        onTap: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ChatDetailPage(user: userSummary);
          }));
        },
      ),
    );
  }
}
