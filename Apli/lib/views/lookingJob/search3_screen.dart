import 'package:login_signup/models/job.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:login_signup/services/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Search3Screen extends StatefulWidget {
  const Search3Screen({Key? key}) : super(key: key);

  @override
  _Search3ScreenState createState() => _Search3ScreenState();
}

class _Search3ScreenState extends State<Search3Screen> {
  final storageToken = new FlutterSecureStorage();
  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text('Search');
  int index = 0;
  int _selectedIndex = 0;
  PageController pageController = PageController();
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Messages',
      style: optionStyle,
    ),
    Text(
      'Index 2: Profile',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      pageController.jumpToPage(index);
    });
  }

  static const List _pages = <Widget>[
    Icon(
      Icons.call,
      size: 150,
    ),
    Icon(
      Icons.camera,
      size: 150,
    ),
    Icon(
      Icons.chat,
      size: 150,
    ),
  ];

  Row jobsCard1(Jobs3Service jobsService, Job act) {
    String? image = act.image;
    String time = act.time;
    String description = act.description;

    index += 1;
    if (index >= 6) {
      index = 0;
    }
    return Row(children: [
      Container(
        width: MediaQuery.of(context).size.width.round() * 0.47,
        height: MediaQuery.of(context).size.height >
                MediaQuery.of(context).size.width
            ? MediaQuery.of(context).size.height.round() * 0.28
            : MediaQuery.of(context).size.height.round() * 0.52,
        padding: new EdgeInsets.all(10.0),
        child: GestureDetector(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            color: AppColors.kLightOrange,
            elevation: 10,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(act.name,
                      style: TextStyle(fontSize: 18.0),
                      textAlign: TextAlign.center),
                ),
                image != null
                    ? CachedNetworkImage(
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        imageUrl: image.toString(),
                        height: 70)
                    : Image.asset("image/no-photo.png", height: 70),
                const SizedBox(
                  height: 10,
                ),
                customText(
                    txt: "- $time",
                    style: const TextStyle(
                        fontSize: 15, color: AppColors.kBlackColor)),
                customText(
                    txt: "- $description",
                    style: const TextStyle(
                        fontSize: 15, color: AppColors.kBlackColor))
              ],
            ),
          ),
          onTap: () async {
            final String? UID = await storageToken.read(key: 'UID');
            jobsService.selectedJob = act.copy();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => Job3DetailScreen(user_id: UID)));
          },
        ),
      ),
    ]);
  }

  Row jobsCard2(Jobs3Service jobsService, Job act, Job act2) {
    String? image = act.image;
    String time = act.time;
    String description = act.description;
    String? image2 = act2.image;
    String time2 = act2.time;
    String description2 = act2.description;
    index += 2;
    if (index >= 6) {
      index = 0;
    }
    return Row(children: [
      Container(
        width: MediaQuery.of(context).size.width.round() * 0.47,
        height: MediaQuery.of(context).size.height >
                MediaQuery.of(context).size.width
            ? MediaQuery.of(context).size.height.round() * 0.28
            : MediaQuery.of(context).size.height.round() * 0.52,
        padding: new EdgeInsets.all(10.0),
        child: GestureDetector(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            color: AppColors.kLightOrange,
            elevation: 10,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(act.name,
                      style: TextStyle(fontSize: 18.0),
                      textAlign: TextAlign.center),
                ),
                image != null
                    ? CachedNetworkImage(
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        imageUrl: image.toString(),
                        height: 70)
                    : Image.asset("image/no-photo.png", height: 70),
                const SizedBox(
                  height: 10,
                ),
                customText(
                    txt: "- $time",
                    style: const TextStyle(
                        fontSize: 15, color: AppColors.kBlackColor)),
                customText(
                    txt: "- $description",
                    style: const TextStyle(
                        fontSize: 15, color: AppColors.kBlackColor))
              ],
            ),
          ),
          onTap: () async {
            final String? UID = await storageToken.read(key: 'UID');
            jobsService.selectedJob = act.copy();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => Job3DetailScreen(user_id: UID)));
          },
        ),
      ),
      Container(
          width: MediaQuery.of(context).size.width.round() * 0.47,
          height: MediaQuery.of(context).size.height >
                  MediaQuery.of(context).size.width
              ? MediaQuery.of(context).size.height.round() * 0.28
              : MediaQuery.of(context).size.height.round() * 0.52,
          padding: new EdgeInsets.all(10.0),
          child: GestureDetector(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: AppColors.kLightOrange,
              elevation: 10,
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(act2.name,
                        style: TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.center),
                  ),
                  image2 != null
                      ? CachedNetworkImage(
                          placeholder: (context, url) =>
                              const CircularProgressIndicator(),
                          imageUrl: image2.toString(),
                          height: 70)
                      : Image.asset("image/no-photo.png", height: 70),
                  const SizedBox(
                    height: 10,
                  ),
                  customText(
                      txt: "- $time2",
                      style: const TextStyle(
                          fontSize: 15, color: AppColors.kBlackColor)),
                  customText(
                      txt: "- $description2",
                      style: const TextStyle(
                          fontSize: 15, color: AppColors.kBlackColor))
                ],
              ),
            ),
            onTap: () async {
              final String? UID = await storageToken.read(key: 'UID');
              jobsService.selectedJob = act2.copy();
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Job3DetailScreen(user_id: UID)));
            },
          ))
    ]);
  }

  Container jobsCard(Jobs3Service jobsService, List jobs) {
    List<Row> cont = [];

    for (var i = 0;
        i < jobs.length;
        jobs.length % 2 == 1 && i == jobs.length - 1 ? i += 1 : i += 2) {
      if (i == jobs.length - 1 && jobs.length % 2 != 0) {
        cont.add(jobsCard1(jobsService, jobs[i]));
      } else {
        cont.add(jobsCard2(jobsService, jobs[i], jobs[i + 1]));
      }
    }
    List<Widget> aux = [];
    for (var i = 0; i < cont.length; i++) {
      aux.add(cont[i]);
    }
    return Container(child: Column(children: aux));

    index += 1;
    if (index >= 6) {
      index = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    final jobsService = Provider.of<Jobs3Service>(context, listen: true);

    //Controller

    if (jobsService.isLoading) return LoadingScreen();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0x759cbf),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                if (customIcon.icon == Icons.search) {
                  customIcon = const Icon(Icons.cancel);
                  customSearchBar = const ListTile(
                    leading: Icon(
                      Icons.search,
                      color: Colors.white,
                      size: 28,
                    ),
                    title: TextField(
                      decoration: InputDecoration(
                        hintText: 'search...',
                        hintStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontStyle: FontStyle.italic,
                        ),
                        border: InputBorder.none,
                      ),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  );
                } else {
                  customIcon = const Icon(Icons.search);
                  customSearchBar = const Text('Search');
                }
              });
            },
            icon: const Icon(Icons.search),
          )
        ],
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 10),
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Row(
                    children: [
                      Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                        return model.isOnline
                            ? FlatButton.icon(
                                icon: Icon(Icons.refresh),
                                label: Text("Clic for refresh the jobs"),
                                onPressed: () {
                                  Navigator.pushReplacementNamed(
                                      context, 'jobs');
                                },
                              )
                            : Text("");
                      }),
                    ],
                  ),
                  Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                    return model.isOnline
                        ? Text("")
                        : AlertDialog(
                            title: Text("Out of internet connection"),
                            content: Text("The information may be outdated"),
                            elevation: 24.0,
                            backgroundColor: Colors.orange,
                          );
                  }),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    "image/WorkappLogo.png",
                    height: 50,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.height >
                            MediaQuery.of(context).size.width
                        ? 20
                        : 210,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: customText(
                        txt: "  LOOK FOR A JOB",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          color: AppColors.kOrangeColor,
                        ),
                        align: TextAlign.center),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                ],
              ),
              customText(
                  txt: "Of your interest",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: AppColors.kBlackColor)),
              jobsCard2(jobsService, jobsService.jobs[0], jobsService.jobs[1]),
              const SizedBox(
                height: 10,
              ),
              customText(
                  txt: "Explore All",
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  )),
              jobsCard(jobsService, jobsService.jobs)
            ],
          ),
        ),
      ),
    );
  }
}
