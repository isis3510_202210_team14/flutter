import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:login_signup/services/services.dart';

class Job2DetailScreen extends StatelessWidget {
  final String? user_id;

  const Job2DetailScreen({Key? key, this.user_id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final jobsService = Provider.of<Jobs2Service>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0x759cbf),
        automaticallyImplyLeading: false,
        title: const Text("Detail"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 10),
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                    return model.isOnline
                        ? Text("")
                        : AlertDialog(
                            title: Text("Out of internet connection"),
                            content: Text(""),
                            elevation: 24.0,
                            backgroundColor: Colors.orange,
                          );
                  }),
                ],
              ),
              Column(
                children: [
                  Stack(
                    children: [
                      JobDetailWidget(image: jobsService.selectedJob.image),
                      Positioned(
                          top: 10,
                          left: 10,
                          child: IconButton(
                            onPressed: () => Navigator.of(context).pop(),
                            icon: Icon(
                              Icons.arrow_back_ios_new,
                              size: 40,
                              color: Colors.black,
                            ),
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  customText(
                      txt: jobsService.selectedJob.name,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: AppColors.kBlackColor)),
                  const SizedBox(
                    height: 8,
                  ),
                  customText(
                      txt: "Salary: \$" +
                          jobsService.selectedJob.salary.toString(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: AppColors.kBlackColor)),
                  const SizedBox(
                    height: 8,
                  ),
                  customText(
                      txt: "Time: " + jobsService.selectedJob.time,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: AppColors.kBlackColor)),
                  const SizedBox(
                    height: 15,
                  ),
                  customText(
                      txt: jobsService.selectedJob.applicants == null
                          ? "There is no applications for this job"
                          : "There are " +
                              jobsService.selectedJob.applicants!.length
                                  .toString() +
                              " applications",
                      style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 20,
                          color: AppColors.kBlackColor)),
                  const SizedBox(
                    height: 8,
                  ),
                  Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                    return model.isOnline
                        ? (jobsService.selectedJob.applicants == null) ||
                                (!jobsService.selectedJob.applicants!
                                    .contains(user_id))
                            ? InkWell(
                                child:
                                    SignUpContainer(st: "Apply for this job!"),
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          const Applied2Screen()));
                                  jobsService.saveNewApplicant(
                                      jobsService.selectedJob);
                                },
                              )
                            : Text("You have already apply for this job!")
                        : Text(
                            "WARN! Connect to internet to apply for the job");
                  }),
                  const SizedBox(
                    height: 8,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
