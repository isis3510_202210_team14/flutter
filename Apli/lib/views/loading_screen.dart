import 'package:login_signup/utils/exports.dart';
import 'package:login_signup/services/services.dart';
import 'package:provider/provider.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final connection = Provider.of<ConnectionStatusModel>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text('Cargando...'),
        ),
        body: Center(
            child: Column(
          children: [
            const SizedBox(
              height: 200,
            ),
            Consumer<ConnectionStatusModel>(builder: (_, model, __) {
              return Center(
                child: !model.isOnline
                    ? Center(
                        child: Column(
                        children: [
                          CircularProgressIndicator(
                            color: Colors.indigo,
                          ),
                          Center(
                              child: Text(
                            'Oh oh...it seems there is no conection',
                            style: Theme.of(context).textTheme.headline3,
                            textAlign: TextAlign.center,
                          )),
                        ],
                      ))
                    : CircularProgressIndicator(
                        color: Colors.indigo,
                      ),
              );
            }),
          ],
        )));
  }
}
