import 'package:login_signup/providers/login_form_provider.dart';
import 'package:login_signup/services/services.dart';
import 'package:login_signup/utils/exports.dart';
import 'package:provider/provider.dart';
import 'package:login_signup/services/auth_service.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Consumer<ConnectionStatusModel>(builder: (_, model, __) {
                      return !model.isOnline
                          ? AlertDialog(
                              title: const Text('Connection failed'),
                              content:
                                  const Text('Check your internet connection'),
                            )
                          : Text("");
                    }),
                    Row(
                      children: [
                        Image.asset(
                          "image/Workapp.png",
                          width: 100,
                        ),
                        const SizedBox(width: 20),
                        customText(
                            txt: "WorkAPP",
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 35,
                            )),
                      ],
                    ),

                    const SizedBox(
                      height: 20,
                    ),
                    customText(
                        txt: "¡Find the ideal job for you!",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        )),
                    const SizedBox(
                      height: 8,
                    ),
                    ChangeNotifierProvider(
                        create: (_) => LoginFormProvider(),
                        child: _LoginForm()),

                    const SizedBox(
                      height: 50,
                    ),

                    InkWell(
                      child: RichText(
                        text: RichTextSpan(
                            one: "Don’t have an account ? ", two: "Sign Up"),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const LoginScreen()));
                      },
                    ),
                    //Text("data"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);
    return Container(
        child: Form(
            key: loginForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                CustomTextFieldReg(
                    Lone: "Email", Htwo: "Your email", LoginForm: loginForm),
                const SizedBox(height: 20),
                CustomTextFieldReg(
                    Lone: "Password",
                    Htwo: "Password",
                    obs: true,
                    LoginForm: loginForm),
                const SizedBox(height: 20),
                InkWell(
                  child: SignUpContainer(st: "Sign in"),
                  onTap: () async {
                    final authService =
                        Provider.of<AuthService>(context, listen: false);

                    loginForm.isLoading = true;

                    final String? errorMessage = await authService.login(
                        loginForm.email, loginForm.password);
                    if (errorMessage == null) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const WelcomeScreen()));
                    } else {
                      if (errorMessage == "INVALID_EMAIL") {
                        NotificationsService.showSnackBar('Invalid email');
                      } else if (errorMessage == "MISSING_PASSWORD") {
                        NotificationsService.showSnackBar('Missing password');
                      } else if (errorMessage == "EMAIL_NOT_FOUND") {
                        NotificationsService.showSnackBar('Email not found');
                      } else if (errorMessage == "INVALID_PASSWORD") {
                        NotificationsService.showSnackBar('Invalid password');
                      } else {
                        NotificationsService.showSnackBar(errorMessage);
                      }
                    }
                  },
                ),
              ],
            )));
  }
}
